<?php
return [

    'app_id' => trim(env('APP_URL', null), '/'),

    'key' => env('SSO_ADMIN_APP_KEY', 'base64:vzEoGsPGfjbtCvov5PDls5ccs6Pk4Y9BSp3mGsQRC/U='),

    'url' => env('SSO_ADMIN_URL', 'https://admin.globalgarner.com/'),

    'session_domain' => env('SSO_ADMIN_SESSION_DOMAIN', '.globalgarner.com'),

    'cookies_name' => env('SSO_ADMIN_COOKIES_NAME', 'admin'),

    'controller_name_space' => 'App\Http\Controllers',

    'dashboard_url' => env('SSO_ADMIN_DASHBOARD_URL', env('APP_URL')),

];