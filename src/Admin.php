<?php
namespace Laukikpatel\SSOAdmin;

use Route;
use Config;
use Illuminate\Support\Str;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Redis;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Cookie\CookieJar as GuzzleHttpCookie;

class Admin
{

    protected $user;

    protected $permissions;

    public function __construct()
    {
        try {

            $cookies_name = config('sso-admin.cookies_name', null);

            if(!$cookies_name) {
                throw new \Exception("SSO_ADMIN_COOKIES_NAME not defined in .env");
            }

            if(isset($_COOKIE[$cookies_name]) && $_COOKIE[$cookies_name]) {

                $encryptDecryptKey = config('sso-admin.key', null);

                if(!$encryptDecryptKey) {
                    throw new \Exception("SSO_ADMIN_APP_KEY not defined in .env");
                }

                if (Str::startsWith($encryptDecryptKey, 'base64:')) {
                    $encryptDecryptKey = base64_decode(substr($encryptDecryptKey, 7));
                }

                $newEncrypter = new Encrypter($encryptDecryptKey, Config::get('app.cipher'));
                $accountCookie = $newEncrypter->decrypt( $_COOKIE[$cookies_name] );

                $user = Redis::get('laravel:'.$accountCookie);
                $data = unserialize(unserialize($user));

                $this->user = (isset($data['admin_user']) && $data['admin_user']) ? (array) $data['admin_user'] : [];
                $this->permissions = (isset($data['admin_user_permissions']) && $data['admin_user_permissions']) ? (array) $data['admin_user_permissions'] : [];
            }

        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }
    }

    /**
     * Check user is logged in or not
     *
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->user ? true : false;
    }

    protected function getCurrentAppPermissions() {
        return (isset($this->permissions[config('sso-admin.app_id', null)]) && is_array($this->permissions[config('sso-admin.app_id', null)])) ? $this->permissions[config('sso-admin.app_id', null)] : [];
    }

    public function isAdmin()
    {
        if(array_key_exists('*', $this->permissions)) {
            return true;
        }

        if(in_array('*@*', $this->getCurrentAppPermissions())) {
            return true;
        }

        return false;

    }

    public function can( $controller, $method = null )
    {
        if($this->isAdmin()) {
            return true;
        }

        if(is_array($controller)) {
            foreach($controller as $c) {
                if(is_array($c)) {
                    if($this->hasPermission($c[0], $c[1])) {
                        return true;
                    }
                } else {
                    $controllerMethod = explode('@', $c);
                    if($this->hasPermission($controllerMethod[0], $controllerMethod[1])) {
                        return true;
                    }
                }
            }
        } else if($method == null) {
            $controller = explode('@', $controller);
            if($this->hasPermission($controller[0], $controller[1])) {
                return true;
            }
        } else {
            if($this->hasPermission($controller, $method)) {
                return true;
            }
        }

        return false;

    }

    private function hasPermission( $controller, $method = '*' ) {
        $controllerNameSpace = config('sso-admin.controller_name_space', 'App\Http\Controllers');
        $controller = ltrim(str_replace($controllerNameSpace, '', $controller), '\\');
        $permission = $controllerNameSpace . '\\' . $controller . '@';
        $currentAppPermissions = $this->getCurrentAppPermissions();
        return ( in_array($permission.'*', $currentAppPermissions) || in_array($permission.$method, $currentAppPermissions) );
    }

    /**
     * Get Logged User Data
     *
     * @return User|null
     */
    public function user()
    {
        return new User($this->user);
    }


    /**
     * Get Login page URI
     *
     * @return string
     */
    public function loginURI()
    {
        return route('ssoAdminLogin');
    }

    /**
     * Get Logout page URI
     *
     * @return string
     */
    public static function logoutURI()
    {
        return route('ssoAdminLogout');
    }

    /**
     * Get Profile page URI
     *
     * @return string
     */
    public function profilePageURI()
    {
        return route('ssoAdminProfile');
    }


    /**
     * Get Application All Permissions from Controllers
     *
     * @return array
     */
    public static function getPermissions()
    {
        $controllers = [];

        foreach (Route::getRoutes()->getRoutes() as $route)
        {
            $action = $route->getAction();

            if (array_key_exists('controller', $action))
            {
                list($rController, $rControllerMethod) = explode('@', $action['controller']);

                if(!array_key_exists($rController, $controllers)) {
                    if(property_exists($rController, 'permissions')) {
                        $controllers[$rController] = $rController::$permissions;
                    }
                }
            }
        }

        return $controllers;
    }

    /**
     * Make a Request
     *
     * @param $method
     * @param $url
     * @param array $data
     * @param array $options
     * @return \Psr\Http\Message\ResponseInterface
     */
    public static function request( $method, $url, array $data = [], array $options = [] )
    {
        if(in_array(strtolower($method), ['post', 'put', 'delete'])) {
            $data = ['form_params' => $data] + $options;
        }

        $cookies_name = config('sso-admin.cookies_name', null);
        $cookies_domain = config('sso-admin.session_domain', null);

        if($cookies_name && $cookies_domain) {
            $jar = GuzzleHttpCookie::fromArray([$cookies_name => $_COOKIE[$cookies_name]], $cookies_domain);
            $data['cookies'] = $jar;
        }

        $http = new GuzzleHttpClient;
        return $http->request( $method, $url, $data );
    }
}