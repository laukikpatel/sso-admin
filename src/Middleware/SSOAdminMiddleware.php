<?php

namespace Laukikpatel\SSOAdmin\Middleware;

use Admin;
use Closure;
use Illuminate\Http\Request;

class SSOAdminMiddleware
{

    public function __construct( Request $request )
    {
        $request->header('Access-Control-Allow-Origin', '*');
        $request->header('Access-Control-Allow-Credentials', 'true');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentRouteAction = \Route::currentRouteAction();

        if(Admin::isLoggedIn()) {

            $canAccess = true;
            if($currentRouteAction !== null) {
                list($controller, $method) = explode('@', $currentRouteAction);
                $canAccess = Admin::can($controller, $method);
            }

            if($canAccess) {
                return $next($request);
            }

        }


        if($request->acceptsHtml() && !$request->ajax()) {

            if(Admin::isLoggedIn()) {
                abort(401, 'Unauthorized');
            } else {
                return redirect(trim(config('sso-admin.url'), '/') . '/login?continue=' . urlencode($request->fullUrl()));
            }

        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

    }
}