<?php

Route::group(['namespace' => 'Laukikpatel\SSOAdmin\Http\Controllers'], function () {

    Route::get('/laukikpatel/sso-admin/controllers', 'AdminController@getControllerList')
        ->middleware('sso-admin');

    Route::get('/sso-admin/my-profile', 'AdminController@Profile')
        ->name('ssoAdminProfile')
        ->middleware('sso-admin');

    Route::get('/sso-admin/login', 'AdminController@Login')
        ->name('ssoAdminLogin');

    Route::get('/sso-admin/logout', 'AdminController@Logout')
        ->name('ssoAdminLogout')
        ->middleware('web');
});