<?php

namespace Laukikpatel\SSOAdmin\Http\Controllers;

use Laukikpatel\SSOAdmin\Admin;

//use Illuminate\Routing\Controller;

class AdminController
{
    public function Login()
    {
        if (Admin::isLoggedIn()) {
            return redirect(config('sso-admin.dashboard_url', '/'));
        }

        $redirectBackURI = request()->headers->get('referer');
        if (!$redirectBackURI) {
            $redirectBackURI = config('sso-admin.dashboard_url', '/');
        }
        return redirect(trim(config('sso-admin.url'), '/') . '/login?continue=' . urlencode($redirectBackURI));
    }

    public function Logout()
    {
        $redirectBackURI = request()->headers->get('referer');
        if (!$redirectBackURI) {
            $redirectBackURI = config('sso-admin.dashboard_url', '/');
        }
        return redirect(trim(config('sso-admin.url'), '/') . '/logout?continue=' . urlencode($redirectBackURI));
    }

    public function getControllerList()
    {
        $permissions = Admin::getPermissions();
        return response()->json($permissions);
    }

    public function Profile()
    {
        return redirect(trim(config('sso-admin.url'), '/') . '/my-profile');
    }
}