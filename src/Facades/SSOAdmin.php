<?php
namespace Laukikpatel\SSOAdmin\Facades;

use Illuminate\Support\Facades\Facade;

class SSOAdmin extends Facade {

    protected static function getFacadeAccessor() {
        return 'Admin';
    }

}