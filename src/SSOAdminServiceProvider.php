<?php

namespace Laukikpatel\SSOAdmin;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class SSOAdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/sso-admin.php', 'sso-admin'
        );

        $this->app->singleton('Admin', function () {
            return new Admin();
        });

        $this->app->alias('Admin', Admin::class);

        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');

        if (version_compare($this->app->version(), '5.4.0') >= 0) {
            $router->aliasMiddleware('sso-admin', 'Laukikpatel\SSOAdmin\Middleware\SSOAdminMiddleware');
        } else {
            $router->middleware('sso-admin', 'Laukikpatel\SSOAdmin\Middleware\SSOAdminMiddleware');
        }
    }

}