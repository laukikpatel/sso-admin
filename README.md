# SSO Admin #

### How do I get set up? ###

Add bellow lines in your composer.json file to register sso admin repository for composer

    "repositories": [
        {
            "type": "vcs",
            "url": "https://laukikpatel@bitbucket.org/laukikpatel/sso-admin.git"
        }
    ],
    "require": {
        "laukikpatel/sso-admin": "dev-master#1.*"
    }

After adding above lines run bellow command to install sso admin package

    composer install
    
After installation completed add bellow line in your config/app.php to register providers and aliases

    'providers' => [
        ...
        Laukikpatel\SSOAdmin\SSOAdminServiceProvider::class
    ],
    
    'aliases' => [
        ...
        'Admin' => Laukikpatel\SSOAdmin\Facades\SSOAdmin::class
    ],

### Environment variable ###

Add bellow lines in your .env file

    SSO_ADMIN_APP_KEY=App key of Admin App
    SSO_ADMIN_URL=https://admin.globalgarner.com/
    SSO_ADMIN_SESSION_DOMAIN=.globalgarner.com
    SSO_ADMIN_COOKIES_NAME=admin
    SSO_ADMIN_DASHBOARD_URL=/home

### Middleware ###

Use middleware for check Authentication

**Example:**

    Route::get('/', 'Controller@method')->middleware('sso-admin');
    
    Route::group(['middleware' => ['sso-admin']], function () {
    
        Route::get('/', 'Controller@method');
        Route::get('/xxx', 'Controller@method');
        
    });


### Define permissions in controller ###

    class YourController extends Controller
    {
    
        public static $permissions = [
            'Permission Title1' => ['method1'],
            'Permission Title2' => ['method2', 'method3']
        ];
    
    
### Helper ###

    Check User is logged In or not
    Admin::isLoggedIn()
    
    Get Logged User All Data
    Admin::user()

    Get Logged User UserID
    Admin::user()->id
    
    Check logged user is Admin
    Admin::isAdmin()
    
    Check Logged user has Permissions
    Admin::can('ControllerName', 'methodName')
    
    if you controller is insdie admin directory
    Admin::can('Admin\ControllerName', 'methodName')
    
    Check permission With single parameter
    Admin::can('ControllerName@methodName')
    
    Check permission With or condition
    Admin::can(['ControllerName@methodName', 'ControllerName@methodName']);
    
    
    Get Login URI
    Admin::loginURI()
    
    Get Logout URI
    Admin::logoutURI()
    
    Get Profile Page URI
    Admin::profilePageURI()
    
    Make a Get Request with SSO Admin
    Admin::request('GET', $url, ['headers' => ['xxx' => 'xxx'])
    
    Make a POST Request with SSO Admin
    Admin::request('POST', $url, ['field' => 'value'], ['headers' => ['xxx' => 'xxx'])
    
    Make PUT Request with SSO Admin
    Admin::request('PUT', $url, ['field' => 'value'], ['headers' => ['xxx' => 'xxx'])
    
    Make DELETE Request with SSO Admin
    Admin::request('DELETE', $url, ['field' => 'value'], ['headers' => ['xxx' => 'xxx'])

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact